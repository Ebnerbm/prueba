
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import vista.TareaFrame;
import modelo.Persona;

/**
 *
 * @author Chino14
 */
public class PersonaControlador implements ActionListener, FocusListener{
    TareaFrame personFrame;
    JComboBox dComboBox;
    JComboBox mComboBox;
    JFileChooser d;
    Persona person;
    String[] departments = new String[] { 
        "Boaco","Carazo","Chinandega","Chontales",
        "Costa Caribe Norte","Costa Caribe Sur",
        "Estelí","Granada","Jinotega","León",
        "Madriz","Managua","Masaya","Matagalpa",
        "Nueva Segovia","Río San Juan","Rivas" };
    String[] masayaMunicipalities = new String[] { 
        "Masaya","Nindirí", "Catarina", "Masatepe", 
        "La concepción", "Tisma","Nandasmo", 
        "San Juan de Oriente" };
    String[] managuaMunicipalities = new String[] { 
        "Managua","Ciudad Sandino", "El crucero", 
        "Mateare", "San Francisco Libre", 
        "San Rafael del Sur","Ticuantepe", "Tipitapa" };
    String[] granadaMunicipalities = new String[] { 
        "Granada","Diriomo", "Diriá", "Nandaime"};
    String[] leonMunicipalities = new String[] { 
        "León","Achuapa", "El Jicaral", 
        "La Paz Centro","Larreynaga",
        "Nagarote","Quezalguaque"};
    String[] rivasMunicipalities = new String[] { 
        "Rivas","Altagracia", "Belén", "Buenos Aires",
        "Cárdenas","Moyogalpa","Potosí", "San Jorge"};
    String[] boacoMunicipalities = new String[] { 
       "Boaco","Camoapa","San José de los Remates","San Lorenzo",
        "Santa Lucía","Teustepe"};
    String[] carazoMunicipalities = new String[]{
        "Jinotepe","Diriamba","Dolores","El rosario",
        "La conquista","La paz","San Marco","Santa Teresa"};
    String[]chinandegaMunicipalities = new String[]{
         "Chinandega","Chichigalpa","Corinto","El realejo",
         "El viejo","Posoltega","Puerto Morazán","San Francisco del Norte",
         "San Juan de Cinco Pinos","San Pedro del Norte",
         "Santo Tomás del Norte","Somotillo","Villanueva"
        };
    String[] chontalesMunicipalities = new String[]{
         "Juigalpa","Acoyapa","Comalapa","El coral",
         "La libertad","San Francisco de Coapa","San Pedro de Lóvago",
         "Santo Domingo","Santo Tomás","Villa Sandino"
        };
    String[] costaCaribeNorteMunicipalities = new String[]{
         "Puerto Cabezas","Siuna","Waspan","Prinzapolka",
         "Rosita","Bonanza","Waslala","Kruskira","Dhaku"};
    String[] costaCaribeSurMunicipalities = new String[]{
         "Bluefields","Corn Island","Desembocadura de Río Grande",
         "El Ayote","El Tortuguero","El Rama","Kukra Hill",
         "La Cruz de Río Grande","Laguna de Perlas",
         "Muelle de los Bueyes","Nueva Guinea","Paiwas"};
    String[] esteliMunicipalities = new String[]{
         "Condega","Estelí","La Trinidad","Pueblo Nuevo",
         "San Juan de Limay","San Nicolás"};
    String[] jinotegaMunicipalities = new String[]{
         "Jinotega","El Cuá","La Concordia","San José de Bocay",
         "San Rafael del Norte","San Sebastián de Yalí",
         "Santa María de Pantasma","Wiwilí"};
    String[] madrizMunicipalities = new String[]{
         "Somoto","Las Sabanas","Palacagüina","San José de Cusmapa",
         "San Juan de Río Coco","San Lucas","Telpaneca",
         "Totogalpa","Yalagüina"};
    String[] matagalpaMunicipalities = new String[]{
         "Matagalpa","Ciudad Darío","El Tuma – La Dalia",
          "Esquipulas","Matiguás","Muy Muy","Rancho Grande",
         "Río Blanco","San Dionisio","San Isidro",
         "San Ramón","Sébaco"};
    String[] nuevaSegoviaMunicipalities = new String[]{
         "Ocotal","Ciudad Antigua","Dipilto","El Jícaro",
          "Jalapa","Macuelizo","Mozonte","Murra",
         "Quilalí","San Fernando","Santa María",
         "Wiwilí"};
    String[] rioSanJuanMunicipalities = new String[]{
         "San Carlos","El Almendro","El Castillo","Morrito",
          "San Juan del Norte","San Miguelito"};
    
    DefaultComboBoxModel departmentComboBoxModel = new DefaultComboBoxModel<>(departments);
    
    public PersonaControlador(TareaFrame f){
        super();
        personFrame = f;
        d = new JFileChooser();
        person = new Persona();
    }
   
    public void setPerson(Persona b){
        person = b;
    }
    public void setPerson(String filePath){
        File f = new File(filePath);
        readPerson(f);
    }
    public Persona getPerson(){
        return person;
    }     
    public void setDepartmentComboBox(JComboBox jcombo){
        dComboBox = jcombo;
        dComboBox.setModel(departmentComboBoxModel);
    }
    public void setMunicipalityComboBox(JComboBox jcombo){
        mComboBox = jcombo;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().getClass().getName().equals(JComboBox.class.getName()))
        {
            JComboBox cbo = (JComboBox)e.getSource();
            switch(cbo.getName()){
                case "department":
                    switch(cbo.getSelectedItem().toString()){
                        case "Managua":
                            mComboBox.setModel(new DefaultComboBoxModel(managuaMunicipalities));
                            break;
                        case "Masaya":
                            mComboBox.setModel(new DefaultComboBoxModel(masayaMunicipalities));
                            break;
                        case "Granada":
                            mComboBox.setModel(new DefaultComboBoxModel(granadaMunicipalities));
                            break;
                        case "León":
                            mComboBox.setModel(new DefaultComboBoxModel(leonMunicipalities));
                            break;
                        case "Rivas":
                            mComboBox.setModel(new DefaultComboBoxModel(rivasMunicipalities));
                            break;
                        case "Boaco":
                            mComboBox.setModel(new DefaultComboBoxModel(boacoMunicipalities));
                            break;
                        case "Carazo":
                            mComboBox.setModel(new DefaultComboBoxModel(carazoMunicipalities));
                            break;
                        case "Chinandega":
                            mComboBox.setModel(new DefaultComboBoxModel(chinandegaMunicipalities));
                            break;
                        case "Chontales":
                            mComboBox.setModel(new DefaultComboBoxModel(chontalesMunicipalities));
                            break;
                        case "Costa Caribe Norte":
                            mComboBox.setModel(new DefaultComboBoxModel(costaCaribeNorteMunicipalities));
                            break;
                        case "Costa Caribe Sur":
                            mComboBox.setModel(new DefaultComboBoxModel(costaCaribeSurMunicipalities));
                            break;
                        case "Esteli":
                            mComboBox.setModel(new DefaultComboBoxModel(esteliMunicipalities));
                            break;
                        case "Jinotega":
                            mComboBox.setModel(new DefaultComboBoxModel(jinotegaMunicipalities));
                            break;
                        case "Madriz":
                            mComboBox.setModel(new DefaultComboBoxModel(madrizMunicipalities));
                            break;
                        case "Matagalpa":
                            mComboBox.setModel(new DefaultComboBoxModel(matagalpaMunicipalities));
                            break;
                        case "Nueva Segovia":
                            mComboBox.setModel(new DefaultComboBoxModel(nuevaSegoviaMunicipalities));
                            break;
                        case "Río San Juan":
                            mComboBox.setModel(new DefaultComboBoxModel(rioSanJuanMunicipalities));
                            break;
                        default:
                            mComboBox.setModel(new DefaultComboBoxModel(new String[]{}));
                            break;
                    }
                    break;
            }
        }
        else
        {
        switch (e.getActionCommand()){
            case "save":
                d.showSaveDialog(personFrame); 
                person = personFrame.getPersonData();
                writePerson(d.getSelectedFile());
                break;
            case "select":                
                d.showOpenDialog(personFrame);  
                person = readPerson(d.getSelectedFile());
                personFrame.setPersonData(person);
                break;
            case "clear":
                personFrame.clear();
                break;

        }
        }
    }
    private void writePerson(File file){
         try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file)); 
            w.writeObject(getPerson());
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(PersonaControlador.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    private Persona readPerson(File file){
         try{
            ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
            return (Persona)ois.readObject();
        }
        catch(FileNotFoundException e){
            JOptionPane.showMessageDialog(personFrame, e.getMessage(),personFrame.getTitle(),JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(PersonaControlador.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    private Persona[] readPersonList(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
        Persona[] persons;
        try(FileInputStream in = new FileInputStream(file);
            ObjectInputStream s = new ObjectInputStream(in)) {
            persons = (Persona[]) s.readObject();
        }
        return persons;
    }
    @Override
    public void focusGained(FocusEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void focusLost(FocusEvent e) {
        if(e.getOppositeComponent().getClass().getName().endsWith("JTextField")){
            switch(((javax.swing.JTextField) e.getSource()).getName()){
                case "idTextField":
                    System.out.println("Evento capturarado desde: idTextField" + ", valor: " +((javax.swing.JTextField) e.getSource()).getText());
                    break;
                default:
                    System.out.println("Evento captudarado desde: " + ((javax.swing.JTextField) e.getSource()).getName());
                    break;
            }
        }
    } 
}
