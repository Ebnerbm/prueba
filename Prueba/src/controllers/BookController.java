/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import models.Book;
import views.BookFrame;

/**
 *
 * @author William Sanchez
 */
public class BookController implements ActionListener {
    private BookFrame Frame;
    private JFileChooser d;
    Book book;
    public BookController(){  
        Frame = new BookFrame();
        d = new JFileChooser();
    
}
    public BookController(BookFrame f){
        super();
        Frame = f;
        d = new JFileChooser();
        book = new Book();
    }
    
    public void setBook(Book b){
        book = b;
    }
    public void setBook(String filePath){
        File f = new File(filePath);
        readBook(f);
    }
    public Book getBook(){
        return book;
    }    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand())
        {
            case "save":
                d.showSaveDialog(Frame); 
                book = Frame.getBookData();
                writeBook(d.getSelectedFile());
                break;
            case "select":                
                d.showOpenDialog(Frame);  
                book = readBook(d.getSelectedFile());
                Frame.setBookData(book);
                break;
            case "clear":
                Frame.clear();
                break;
            
        }        
    }
    private void writeBook(File file){
         try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file)); 
            w.writeObject(getBook());
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(BookController.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    private Book readBook(File file){
         try{
            ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
            return (Book)ois.readObject();
        }
        catch(FileNotFoundException e){
            JOptionPane.showMessageDialog(Frame, e.getMessage(),Frame.getTitle(),JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(BookController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
